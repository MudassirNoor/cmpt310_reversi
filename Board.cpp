#include "Board.h"
#include <iostream>
using namespace std;

Index::Index(int x_in, int y_in) {
    x = x_in;
    y = y_in;
}

Index::Index() {
    x = -1;
    y = -1;
}

Board::Board() = default;

/**
 * Initializes the member array from a give array
 * @param array
 */
Board::Board(int array[BoardSize][BoardSize]) {
    for (int x = 0; x < BoardSize; x++) {
        for (int y = 0; y < BoardSize; y++) {
            currentBoardArray[x][y] = array[x][y];
        }
    }
}

/**
 * Copy constructor for the board object
 * @param board reference to the board object to copy
 */
Board::Board(const Board &board) {
    for (int x = 0; x < BoardSize; x++) {
        for (int y = 0; y < BoardSize; y++) {
            currentBoardArray[x][y] = board.currentBoardArray[x][y];
        }
    }
}

void Board::display() {
    for (auto & x : currentBoardArray) {
        cout << "|";
        for (int y : x) {
            if (y == 0) {
                cout << "_" << "|";
            }
            else if (y == 1){
            cout << "\033[4;31m" << y << "\033[0m" << "|";
            }
            else{
                cout << "\033[4;36m" << y << "\033[0m" << "|";
            }
        }
        cout << endl;
    }
}

/*
* check area around an empty square, see if it has an enemy piece bordering it 
*/
bool Board::check_adjacent_squares(int row, int col, int player, int enemy) {
    if (row < MAXINDEX) {
        if (currentBoardArray[row + 1][col] == enemy) {
            if (check_right_row(row + 1, col, player)) {
                return true;
            }
        }
    }
    if (row > 0) {
        if (currentBoardArray[row - 1][col] == enemy) {
            if (check_left_row(row - 1, col, player)) {
                return true;
            }
        }
    }
    if (col < MAXINDEX) {
        if (currentBoardArray[row][col + 1] == enemy) {
            if (check_bottom_col(row, col + 1, player)) {
                return true;
            }
        }
    }
    if (col > 0) {
        if (currentBoardArray[row][col - 1] == enemy) {
            if (check_top_col(row, col - 1, player)) {
                return true;
            }
        }
    }
    if (row != MAXINDEX && col != MAXINDEX) {
        if (currentBoardArray[row + 1][col + 1] == enemy) {
            if (check_bottom_right_diag(row + 1, col + 1, player)) {
                return true;
            }
        }
    }
    if (row != 0 && col != 0) {
        if (currentBoardArray[row - 1][col - 1] == enemy) {
            if (check_top_left_diag(row - 1, col - 1, player)) {
                return true;
            }
        }
    }
    if (row != 0 && col != MAXINDEX) {
        if (currentBoardArray[row - 1][col + 1] == enemy) {
            if (check_top_right_diag(row - 1, col + 1, player)) {
                return true;
            }
        }
    }
    if (row != MAXINDEX && col != 0) {
        if (currentBoardArray[row + 1][col - 1] == enemy) {
            if (check_bottom_left_diag(row + 1, col - 1, player)) {
                return true;
            }
        }
    }
    return false;

}

bool Board::check_right_row(int xbase, int ybase, int player) {
    for (int x = xbase; x <= MAXINDEX; x++) {
        if (currentBoardArray[x][ybase] == player) {
            return true;
        }
        if (currentBoardArray[x][ybase] == 0) {
            return false;
        }
    }
    return false;
}

bool Board::check_left_row(int xbase, int ybase, int player) {
    for (int x = xbase; x >= 0; x--) {
        if (currentBoardArray[x][ybase] == player) {
            return true;
        }
        if (currentBoardArray[x][ybase] == 0) {
            return false;
        }
    }
    return false;
}

bool Board::check_top_col(int xbase, int ybase, int player) {
    for (int y = ybase; y >= 0; y--) {
        if (currentBoardArray[xbase][y] == player) {
            return true;
        }
        if (currentBoardArray[xbase][y] == 0) {
            return false;
        }
    }
    return false;
}

bool Board::check_bottom_col(int xbase, int ybase, int player) {
    for (int y = ybase; y <= MAXINDEX; y++) {
        if (currentBoardArray[xbase][y] == player) {
            return true;
        }
        if (currentBoardArray[xbase][y] == 0) {
            return false;
        }
    }
    return false;
}

bool Board::check_bottom_right_diag(int xbase, int ybase, int player) {
    int row = xbase + 1, col = ybase + 1;
    while (row <= MAXINDEX && col <= MAXINDEX) {
        if (currentBoardArray[row][col] == player)
            return true;
        if (currentBoardArray[row][col] == 0)
            return false;

        row++, col++;
    }
    return false;
}

bool Board::check_top_left_diag(int xbase, int ybase, int player) {
    int row = xbase - 1, col = ybase - 1;
    while (row >= 0 && col >= 0) {
        if (currentBoardArray[row][col] == player)
            return true;
        if (currentBoardArray[row][col] == 0)
            return false;

        row--, col--;
    }
    return false;
}

bool Board::check_bottom_left_diag(int xbase, int ybase, int player) {
    int row = xbase + 1, col = ybase - 1;
    while (row <= MAXINDEX && col >= 0) {
        if (currentBoardArray[row][col] == player)
            return true;
        if (currentBoardArray[row][col] == 0)
            return false;

        row++, col--;
    }
    return false;
}

bool Board::check_top_right_diag(int xbase, int ybase, int player) {
    int row = xbase - 1, col = ybase + 1;
    while (row >= 0 && col <= MAXINDEX) {
        if (currentBoardArray[row][col] == player)
            return true;
        if (currentBoardArray[row][col] == 0)
            return false;

        row--, col++;
    }
    return false;
}

VectorWrapper<Index> Board::get_legal_positions(int player) {
    vector<Index> possibleMoves(VECLEN);
    int vecSize = 0;

    int enemy = 0;
    if (player == 1) {
        enemy = 2;
    }
    else {
        enemy = 1;
    }

    for (int row = 0; row < BoardSize; row++) {
        for (int col = 0; col < BoardSize; col++) {
            if (currentBoardArray[row][col] == 0) {
                if (check_adjacent_squares(row, col, player, enemy)) {
                    possibleMoves[vecSize].x = row;
                    possibleMoves[vecSize].y = col;
                    vecSize++;
                }

            }
        }
    }

    VectorWrapper<Index> moves(possibleMoves, vecSize);

    return moves;
}

bool Board::check_draw() {
    // Assuming neither players can make a turn
    int count_1 = 0, count_2 = 0;
    for (auto & x : currentBoardArray) {
        for (int y : x) {
            if (y == 1) count_1++;
            if (y == 2) count_2++;
        }
    }

    return count_1 == count_2 && count_1 != 0;
}

int Board::check_win() {
    // Assuming neither players can make a turn
    int count_1 = 0, count_2 = 0;
    for (auto & x : currentBoardArray) {
        for (int y : x) {
            if (y == 1) count_1++;
            if (y == 2) count_2++;
        }
    }

    if (count_1 > count_2) return 1;
    else if (count_2 > count_1) return 2;
    else return 0;
}

DoublePointer Board::get_board_array() {
    return currentBoardArray;
}

void Board::update_board(int newArray[BoardSize][BoardSize]){
    for (int i = 0; i < BoardSize; i++) {
        for (int j = 0; j < BoardSize; j++) {
            currentBoardArray[i][j] = newArray[i][j];
        }
    }
}

int Board::count_characters(int character) {
    int count = 0;
    for (auto & x: currentBoardArray) {
        for (int y : x) {
            if (y == character) count++;
        }
    }
    return count;
}