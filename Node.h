#ifndef NODE_H
#define NODE_H 

#include "State.h"
#include "VectorWrapper.h"
#include <vector>
using namespace std;

class Node {
public:
    State state;
    Node* parent;
    VectorWrapper<Node> children = VectorWrapper<Node>(0);
    int wins;
    int losses;
	int draws; 
	int corner;
	int corneradjacent; 
	int frontier;
	int stability; 

	Node();
	Node(const State &state_in, Node* parent_in);
	bool is_root();
	bool is_leaf();
	void increment_wins();
	void increment_losses();
	void increment_draws();
	void expand_node();
	Node* get_best_child();
	int get_node_score();
	void reset_node(State state_in);

	Node* get_best_heuristic_child();
	int get_heuristic_score();
	void increment_corner();
	void increment_corner_adjacent();
	void increment_frontier(); 
	void increment_stability(); 
};

int check_stability_top_left_corner(DoublePointer board, int x, int y, int turnPlayer);
int check_stability_bottom_right_corner(DoublePointer board, int x, int y, int turnPlayer);
int check_stability_bottom_left_corner(DoublePointer board, int x, int y, int turnPlayer);
int check_stability_top_right_corner(DoublePointer board, int x, int y, int turnPlayer);
int check_pos_diag(DoublePointer board, int x, int y, int turnPlayer);
int check_neg_diag(DoublePointer board, int x, int y, int turnPlayer);


#endif 