#include "Board.h"
#include "State.h"
#include <iostream>
#include <ctime>
#include "PureMonteCarloTreeSearch.h"

void print_general_statistics(int moveNumber, int emptySlots, int player1, int player2);

int main() {
    int boardArray[BoardSize][BoardSize] =
            {{0, 0, 0, 0, 0, 0, 0, 0},
             {0, 0, 0, 0, 0, 0, 0, 0},
             {0, 0, 0, 0, 0, 0, 0, 0},
             {0, 0, 0, 1, 2, 0, 0, 0},
             {0, 0, 0, 2, 1, 0, 0, 0},
             {0, 0, 0, 0, 0, 0, 0, 0},
             {0, 0, 0, 0, 0, 0, 0, 0},
             {0, 0, 0, 0, 0, 0, 0, 0}};

    Board gameBoard = Board(boardArray);
    State currentState = State(gameBoard, 1, 2);
    Node currentNode = Node(currentState, nullptr);

    cout << "========================================================\n";
    cout << "REVERSI with Pure Monte Carlo Tree Search\n";
    cout << "========================================================\n";
    cout << "Player 1 uses pure monte carlo tree search.\n";
    cout << "Player 2 uses pure monte carlo tree search with added heuristics.\n";
    cout << "After each play, the following data will be captured for each player:\n";
    cout << "1) Move number\n";
    cout << "2) Number of empty slots\n";
    cout << "3) Player 1 pieces\n";
    cout << "4) Player 2 pieces\n";
    cout << "========================================================\n";

    cout << "Initial game board:\n";
    gameBoard.display();
    cout << endl;

    int status = CONTINUE;
    DoublePointer arr;
    time_t startTime = time(nullptr);
    int moveNumber = 0;
    while (status == CONTINUE) {
        moveNumber++;
        cout << "Player " << currentState.turnPlayer << " is making a move...\n";

        if (currentState.turnPlayer == 1) {
            arr = PureMonteCarloTreeSearch(&currentNode, currentState.turnPlayer);
        }
        else {
            arr = PureMonteCarloTreeSearchHeuristics(&currentNode, currentState.turnPlayer);
        }

        if (arr == nullptr) {
            cout << "Player " << currentState.turnPlayer << " could not make any moves.\n";
            currentState.switch_turnPlayer();
        }
        else {
            currentState.update_state(arr);
            currentState.board.display();
        }

        print_general_statistics(moveNumber, currentState.board.count_characters(0),
                currentState.board.count_characters(1), currentState.board.count_characters(2));

        currentNode.reset_node(currentState);
        cout << endl;

        status = currentState.check_status();
    }
    time_t elapsedTime = time(nullptr) - startTime;

    if (status == 1) {
        cout << "The winner is player 1." << endl;
    }
    else if (status == 2) {
        cout << "The winner is player 2." << endl;
    }
    else {
        cout << "The game ended in a tie." << endl;
    }

    cout << "Total Execution time: " << elapsedTime << endl;
    return 0;
}

void print_general_statistics(int moveNumber, int emptySlots, int player1, int player2) {
    cout << "Move number: " << moveNumber << endl;
    cout << "Number of empty slots: " << emptySlots << endl;
    cout << "Player 1 pieces: " << player1 << endl;
    cout << "Player 2 pieces: " << player2 << endl;
}