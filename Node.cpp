#include "Node.h"
#include <iostream>

using namespace std; 

Node::Node() {
	children = VectorWrapper<Node>(0);
	state = State(); 
	parent = nullptr; 
	wins = 0;
	losses = 0;
	draws = 0;
	corner = 0; 
	frontier = 0;
	stability = 0; 
	corneradjacent = 0; 
}

Node::Node(const State &state_in, Node* parent_in): children(0), state(state_in), parent(parent_in) {
	wins = 0; 
	losses = 0; 
	draws = 0; 
	corner = 0;
	frontier = 0;
	stability = 0;
	corneradjacent = 0;
}

bool Node::is_root() {
    return parent == nullptr;
}

bool Node::is_leaf() {
    return children.size == 0;
}

void Node::increment_wins() {
	wins++; 
}

void Node::increment_losses() {
	losses++; 
}

void Node::increment_draws() {
	draws++; 
}

void Node::expand_node() {
	int i = 0;
	VectorWrapper<State> possibleStates = state.get_possible_states();
	children.resize_wrapper(possibleStates.size);

	for (i = 0; i < possibleStates.size; i++) {
		children.vec[i] = Node(possibleStates.get_vec_data(i), this);
	}
	children.size = i;
}

Node* Node::get_best_child() {
    if (children.size == 0) {
        return nullptr;
    }

	Node* bestNode = &children.vec[0];
	int lastBestScore = bestNode->get_node_score();
	int score = 0;
	int i = 0; 

	for (i = 0; i < children.size; i++) {
		score = children.vec[i].get_node_score();
		if (score > lastBestScore) {
			bestNode = &children.vec[i];
			lastBestScore = score; 
		}
		else if (score == lastBestScore) {
			if (children.vec[i].wins >= bestNode->wins) {
				bestNode = &children.vec[i];
			}
		}
	}

	return bestNode; 
}

int Node::get_node_score() {
	return wins + draws; 
}


void Node::reset_node(State state_in) {
	state = state_in; 
	parent = nullptr; 
	children.size = 0; 
	wins = 0; 
	losses = 0; 
	draws = 0; 
	corner = 0;
	frontier = 0;
	stability = 0;
	corneradjacent = 0;
}

Node* Node::get_best_heuristic_child() {
    if (children.size == 0) {
        return nullptr;
    }

	Node* bestNode = &children.vec[0];
	int lastBestScore = bestNode->get_heuristic_score();
	int score = 0;
	int i = 0;

	for (i = 0; i < children.size; i++) {
		score = children.vec[i].get_heuristic_score();
		if (score > lastBestScore) {
			bestNode = &children.vec[i];
			lastBestScore = score;
		}
		else if (score == lastBestScore) {
			if (children.vec[i].wins >= bestNode->wins) {
				bestNode = &children.vec[i];
			}
		}
	}
	return bestNode;
}

int Node::get_heuristic_score() {
	return corner*100 + corneradjacent*100 + stability*5 + wins + draws; 
}

/*
* always want to take a corner
*/
void Node::increment_corner() {
    int x_lastMoves = state.lastMove.x, y_lastMove = state.lastMove.y;
	if (x_lastMoves == 0 && y_lastMove == 0) {
		corner = 1;
	}
	else if (x_lastMoves == MAXINDEX && y_lastMove == MAXINDEX) {
		corner = 1;
	}
	else if (x_lastMoves == MAXINDEX && y_lastMove == 0) {
		corner = 1;
	}
	else if (x_lastMoves == 0 && y_lastMove == MAXINDEX) {
		corner = 1;
	}
	else {
		corner = 0; 
	}
}

void Node::increment_corner_adjacent() {
	int xLast = state.lastMove.x, yLast = state.lastMove.y;
	DoublePointer board = state.board.get_board_array();
	if ((xLast == 0 && yLast == 1) || (xLast == 1 && yLast == 0) || (xLast == 1 && yLast == 1)) {
		if (board[0][0] == 0) {
			corneradjacent = -1; 
		}
	}
	else if((xLast == 6 && yLast == 0) || (xLast == 6 && yLast == 1) || (xLast == MAXINDEX && yLast == 1)) {
		if (board[MAXINDEX][0] == 0) {
			corneradjacent = -1;
		}
	}
	else if ((xLast == 0 && yLast == 6) || (xLast == 1 && yLast == 6) || (xLast == 1  && yLast == MAXINDEX)) {
		if (board[0][MAXINDEX] == 0) {
			corneradjacent = -1;
		}
	}
	else if ((xLast == 6 && yLast == MAXINDEX) || (xLast == 6 && yLast == 6) || (xLast == MAXINDEX && yLast == 6)) {
		if (board[MAXINDEX][MAXINDEX] == 0) {
			corneradjacent = -1;
		}
	}
	else {
		//do nothing
	}
}

/*
* select move that opens up the least number of moves for enemy
* by having fewest bordering empty spaces
*/
void Node::increment_frontier() {
	DoublePointer board = state.board.get_board_array();
	int x = state.lastMove.x;
	int y = state.lastMove.y;
	int localScore = 0;

	if (x < MAXINDEX) {
		if (board[x + 1][y] == 0) {
			frontier--;
		}
	}
	if (x > 0) {
		if (board[x - 1][y] == 0) {
			frontier--;
		}
	}
	if (y > 0) {
		if (board[x][y - 1] == 0) {
			frontier--;
		}
	}
	if (y < MAXINDEX) {
		if (board[x][y + 1] == 0) {
			frontier--;
		}
	}
	if (x != MAXINDEX && y != MAXINDEX) {
		if (board[x + 1][y + 1] == 0) {
			frontier--;
		}
	}
	if (x != 0 && y != 0) {
		if (board[x - 1][y - 1] == 0) {
			frontier--;
		}
	}
	if (x != 0 && y != MAXINDEX) {
		if (board[x - 1][y + 1] == 0) {
			frontier--;
		}
	}
	if (x != MAXINDEX && y != 0) {
		if (board[x + 1][y - 1] == 0) {
			frontier--;
		}
	}
}

void Node::increment_stability() {
	DoublePointer board = state.board.get_board_array(); 
	Index row0(0,0), row7(0,0), col0(0,0), col7(0,MAXINDEX); 
	int leftCornerStable = 0;
	int i = 0; 
	int x = 0;
	int y = 0;

	if (board[0][0] == state.turnPlayer) {
		for (i = 0; i <= MAXINDEX; i++) {
			if (board[x][0] != state.turnPlayer && board[0][y] != state.turnPlayer) {
				break;
			}
			if (board[x][0] == state.turnPlayer) {
				x++;
			}
			if (board[0][y] == state.turnPlayer) {
				y++;
			}
		}
		stability = stability + x + y - 1;
	}
	row0.y = y; 
	col0.x = x; 

	
	if (x >= 1 && y >= 1) {
		for (int i = 1; i < x; i++) {
			for (int j = 1; j < y; j++) {
				if (board[i][j] == state.turnPlayer) {
					stability = stability + check_stability_top_left_corner(board, i, j, state.turnPlayer);
				}
			}
		}
	}


	x = MAXINDEX; y = MAXINDEX;
	if (board[MAXINDEX][MAXINDEX] == state.turnPlayer) {
		for (i = MAXINDEX; i >= 0; i--) {
			if (board[x][MAXINDEX] != state.turnPlayer && board[MAXINDEX][y] != state.turnPlayer) {
				break;
			}
			if (board[x][MAXINDEX] == state.turnPlayer) {
				x--;
			}
			if (board[MAXINDEX][y] == state.turnPlayer) {
				y--;
			}
		}
		stability = stability + (MAXINDEX - x) + (MAXINDEX - y) - 1;
	}
	row7.y = y; 
	col7.x = x; 

	if (x <= 6 && y <= 6) {
		for (int i = 6; i > x; i--) {
			for (int j = 6; j > y; j--) {
				if (board[i][j] == state.turnPlayer) {
					stability = stability + check_stability_bottom_right_corner(board, i, j, state.turnPlayer);
				}
			}
		}
	}


	//top_right_corner
	x = 0; y = MAXINDEX; 
	if (board[0][MAXINDEX] == state.turnPlayer) {
		while (x <= col7.x || y >= row0.y) {
			if (board[x][MAXINDEX] != state.turnPlayer && board[0][y] != state.turnPlayer) {
				break;
			}
			if (board[x][MAXINDEX] == state.turnPlayer) {
				x++;
			}
			if (board[0][y] == state.turnPlayer) {
				y--;
			}
		}
		stability = stability + x + (MAXINDEX - y) - 1;
	}

	if (x >= 1 && y <= 6) {
		for (int i = 1; i < x; i++) {
			for (int j = 6; j > y; j--) {
				if (board[i][j] == state.turnPlayer) {
					stability = stability + check_stability_top_right_corner(board, i, j, state.turnPlayer);
				}
			}
		}
	}

	//bottom left corner
	x = MAXINDEX; y = 0; 
	if (board[MAXINDEX][0] == state.turnPlayer) {
		while (x >= col0.x || y <= row7.y) {
			if (board[x][0] != state.turnPlayer && board[MAXINDEX][y] != state.turnPlayer) {
				break;
			}
			if (board[x][0] == state.turnPlayer) {
				x--;
			}
			if (board[MAXINDEX][y] == state.turnPlayer) {
				y++;
			}
		}
		stability = stability + (MAXINDEX - x) + y - 1;
	}

	if (x <= 6 && y >= 1) {
		for (int i = 6; i > x; i--) {
			for (int j = 1; j < y; j++) {
				if (board[i][j] == state.turnPlayer) {
					stability = stability + check_stability_bottom_left_corner(board, i, j, state.turnPlayer);
				}
			}
		}
	}
}


int check_stability_top_left_corner(DoublePointer board, int x, int y, int turnPlayer) {
	if (board[x - 1][y - 1] == turnPlayer && board[x - 1][y] == turnPlayer && board[x][y - 1] == turnPlayer) {
		return check_pos_diag(board, x, y, turnPlayer);
	}
	else {
		return 0; 
	}
}

int check_stability_bottom_right_corner(DoublePointer board, int x, int y, int turnPlayer) {
	if (board[x + 1][y + 1] == turnPlayer && board[x + 1][y] == turnPlayer && board[x][y + 1] == turnPlayer) {
		return check_pos_diag(board, x, y, turnPlayer);
	}
	else {
		return 0;
	}
}

int check_stability_bottom_left_corner(DoublePointer board, int x, int y, int turnPlayer) {
	if (board[x + 1][y - 1] == turnPlayer && board[x + 1][y] == turnPlayer && board[x][y - 1] == turnPlayer) {
		return check_neg_diag(board, x, y, turnPlayer);
	}
	else {
		return 0;
	}
}

int check_stability_top_right_corner(DoublePointer board, int x, int y, int turnPlayer) {
	if (board[x - 1][y + 1] == turnPlayer && board[x - 1][y] == turnPlayer && board[x][y + 1] == turnPlayer) {
		return check_neg_diag(board, x, y, turnPlayer);
	}
	else {
		return 0;
	}
}

/*check diagonal on positive slope*/
int check_pos_diag(DoublePointer board, int x, int y, int turnPlayer) {
	int i = x;
	int j = y;
	bool stable1 = true;
	bool stable2 = true;

	while (i >= 0 && j <= MAXINDEX) {
		if (board[i][j] != turnPlayer) {
			stable1 = false;
			break;
		}
		i--;
		j++;
	}

	i = x;
	j = y;
	while (i <= MAXINDEX && j >= 0) {
		if (board[i][j] != turnPlayer) {
			stable2 = false;
			break;
		}
		i++;
		j--;
	}

	if (stable1 || stable2) {
		return 1;
	}
	else {
		return 0;
	}
}

/*check diagonal on negative slope*/
int check_neg_diag(DoublePointer board, int x, int y, int turnPlayer) {
	int i = x;
	int j = y;
	bool stable1 = true;
	bool stable2 = true;

	while (i <= MAXINDEX && j <= MAXINDEX) {
		if (board[i][j] != turnPlayer) {
			stable1 = false;
			break; 
		}

		i++;
		j++;
	}

	i = x;
	j = y; 
	while (i >= 0 && j >= 0) {
		if (board[i][j] != turnPlayer) {
			stable2 = false;
			break;
		}

		i--;
		j--;
	}

	if (stable1 || stable2) {
		return 1;
	}
	else {
		return 0;
	}
}