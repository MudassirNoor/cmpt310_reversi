GCC = g++
DEPS = Board.h State.h Node.h
OBJ = reversi.o Board.o State.o Node.o VectorWrapper.h PureMonteCarloTreeSearch.h

%.o: %.c $(DEPS)
	$(GCC) -c -o $@ $<

reversi: $(OBJ)
	$(GCC) -o $@ $^

clean:
	rm *.o reversi